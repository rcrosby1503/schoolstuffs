#include <iostream>
#include <stdio.h>
#include <fcntl.h>
#include <fstream>
#include <stdlib.h>
#include <unistd.h>
#include <cmath>
#include <linux/stat.h>
#include <sys/stat.h>
#include "ext2_fs.h"

using namespace std;


int main(){
	struct ext2_super_block superBlock;
	struct ext2_group_desc groupBlock;
	struct stat sb;
	int file;
	int inodeNumber;
	char bootSec[1024];
	
	file = open("/home/bob/Desktop/Filesystem/teststuff/test.1k.ext2", O_RDONLY);
	if(file < 0){
		cout << "File failed to open\n";
		return 0;
	}
	//read(file, bootSec, 1024);
	lseek(file, 1024, SEEK_CUR);
	read(file, &superBlock, sizeof(struct ext2_super_block));
	if(fstat(file, &sb) < 0) return 1;
	
	printf("Inode Number: %ld\n\n", sb.st_ino);
	
	printf("Size of the block: %x \n", superBlock.s_log_block_size);
	printf("blocks per group: %d \n", superBlock.s_blocks_per_group);
	printf("Number of blocks: %d \n", superBlock.s_blocks_count);
	printf("Number of Free blocks: %d \n\n", superBlock.s_free_blocks_count);
	
	printf("First Inode: %d\n", superBlock.s_first_ino);
	printf("Number of Inodes: %d \n", superBlock.s_inodes_count);
	printf("Number of free Inodes: %d \n\n", superBlock.s_free_inodes_count);
	
	printf("Fragments per group: %d \n\n", superBlock.s_frags_per_group);
	
	printf("Magic Number: %x \n\n", superBlock.s_magic);
	
	read(file, &groupBlock, sizeof(struct ext2_group_desc));
	printf("Bg block bitmap: %u\n", groupBlock.bg_block_bitmap);
	printf("bg inode table: %u\n", groupBlock.bg_inode_table);
	printf("Used Directories: %u\n", groupBlock.bg_used_dirs_count);
	
	close(file);
	
	return 0;
}
