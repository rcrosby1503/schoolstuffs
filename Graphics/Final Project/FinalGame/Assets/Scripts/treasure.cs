﻿using UnityEngine;
using System.Collections;

public class treasure : MonoBehaviour {
	private bool collected = false;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if (collected) {
			gameObject.active = false;
		}
	}
	

	void OnTriggerEnter(Collider other){
		if (other.gameObject.tag == "Player") {
			collected = true;
		}
	}
}
