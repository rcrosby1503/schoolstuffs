﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

	public AudioClip[] audioClip;
	private int tokenCount = 0;
	private int currentTokens = 0;
	// Use this for initialization
	void Start () {
		Screen.showCursor = false;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other){
		if (other.gameObject.tag == "Token") {
			playSound(0);
			currentTokens++;
			if(currentTokens == 3){
				Time.timeScale = 0;
				Application.LoadLevel("victory");
			}
		}
		if (other.gameObject.tag == "Enemy") {
			Application.LoadLevel("initial");
		}
	}

	void playSound(int clip){
		audio.clip = audioClip [clip];
		audio.Play ();
	}
}
