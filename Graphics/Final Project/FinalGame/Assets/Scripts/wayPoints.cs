﻿using UnityEngine;
using System.Collections;

public class wayPoints : MonoBehaviour {

	public Transform[] waypoint;
	public float patrolSpeed = 6.0f;
	public bool loop = true;
	public float pauseDuration;

	private float curTime;
	private int currentWaypoint = 0;
	private CharacterController character;

	// Use this for initialization
	void Start () {
		character.GetComponent <CharacterController> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (currentWaypoint < waypoint.Length - 1) {
			patrol ();		
		} else {
			if(loop){
				currentWaypoint = 0;
			}				
		}
	}

	void patrol(){
		Vector3 nextWayPoint = waypoint [currentWaypoint].position;
		nextWayPoint.y = transform.position.y;
		Vector3 moveDirection = nextWayPoint - transform.position;

		if (moveDirection.magnitude < 1.5) {
			Debug.Log ("Enemy is close to next way point");

			if(curTime == 0){
				curTime = Time.time;

				if((Time.time - curTime) >= pauseDuration){
					Debug.Log ("Increasing waypoint");
					currentWaypoint++;
					curTime = 0;
				}
			}
		}
		else{
			character.Move (moveDirection.normalized * patrolSpeed * Time.deltaTime);
		}
	}
}
